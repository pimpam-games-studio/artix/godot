# Maintainer: Oscar Campos <oscar.campos@thepimpam.com>
# Based in previous work by:
#   Jorge Araya Navarro <jorgejavieran@yahoo.com.mx>
#   Cristian Porras <porrascristian@gmail.com>
#   Matthew Bentley <matthew@mtbentley.us>

pkgbase=godot
pkgname=(
  godot
  godot-headless-server
  godot-export-templates
)
pkgdesc="An advanced, feature packed, multi-platform 2D and 3D game engine that includes ANL and Voxel support"
pkgver=3.3.2
pkgrel=1
arch=('x86_64')
url="http://www.godotengine.org"
depends=('libxcursor' 'libxinerama' 'freetype2' 'pulseaudio' 'libxrandr' 'libxi' 'libglvnd')
makedepends=('git' 'scons' 'gcc' 'yasm' 'pkgconf')
source=(
  "https://github.com/godotengine/godot/archive/${pkgver}-stable.tar.gz"
  "https://github.com/Zylann/godot_voxel/archive/master.zip"
  "https://github.com/Xrayez/godot-anl/archive/2.1.1-stable.tar.gz"
)
sha256sums=(
    'd705c6932f78da28cc1609537a9c407a89afaf45f2e58555becc0d7f2bcf7aca'
    SKIP
    '8b174216c1d2b20b053f0535305e4f8c36585a2df8076198bdb2b20da92a2122'
)

prepare() {
  mv "godot_voxel-master" "${pkgbase}-${pkgver}-stable/modules/voxel"
  mv "godot-anl-2.1.1-stable" "${pkgbase}-${pkgver}-stable/modules/anl"
  cp -ra "${pkgbase}-${pkgver}-stable"{,-export-templates}
  cp -ra "${pkgbase}-${pkgver}-stable"{,-headless-server}
}

build() {

  msg2 "Building Godot Engine..."
  (cd "${srcdir}"/${pkgbase}-${pkgver}-stable
    scons platform=x11 tools=yes target=release_debug use_llvm=no colored=yes pulseaudio=yes bits=64 -j $((`nproc`+1))
  )

  msg2 "Building Godot Headless Server..."
  (cd "${srcdir}"/${pkgbase}-${pkgver}-stable-headless-server
    scons platform=x11 tools=no target=release bits=64 -j $((`nproc`+1))
    scons platform=x11 tools=no target=release_debug bits=64 -j $((`nproc`+1))
  )

  msg2 "Building Godot Export Templates..."
  (cd "${srcdir}"/${pkgbase}-${pkgver}-stable-export-templates
    scons platform=x11 tools=no target=release pulseaudio=yes bits=64 -j $((`nproc`+1))
    scons platform=x11 tools=no target=release_debug pulseaudio=yes bits=64 -j $((`nproc`+1))
  )
}

package_godot() {
  provides=("${pkgbase}")
  conflicts=("${pkgbase}" "${pkgbase}-git" "${pkgbase}-pulse")
  license=('MIT')

  cd "${srcdir}"

  install -Dm644 "${srcdir}"/${pkgbase}-${pkgver}-stable/misc/dist/linux/org.godotengine.Godot.desktop "${pkgdir}"/usr/share/applications/godot.desktop
  install -Dm644 "${srcdir}"/${pkgbase}-${pkgver}-stable/icon.svg "${pkgdir}"/usr/share/pixmaps/godot.svg

  cd "${srcdir}"/${pkgbase}-${pkgver}-stable

  install -D -m755 bin/godot.x11.opt.tools.64 "${pkgdir}"/usr/bin/godot
  install -D -m644 "${srcdir}"/${pkgbase}-${pkgver}-stable/LICENSE.txt "${pkgdir}"/usr/share/licenses/godot/LICENSE
  install -D -m644 "${srcdir}"/${pkgbase}-${pkgver}-stable/misc/dist/linux/godot.6 "${pkgdir}"/usr/share/man/man6/godot.6
}

package_godot-export-templates() {
  provides=("${pkgbase}-export-templates")
  conflicts=("${pkgbase}-export-templates")
  depends=("${pkgbase}")
  license=('MIT')

  cd "${srcdir}"
  mkdir -p "${pkgdir}"/usr/share/godot/templates/

  install -Dm644 "${srcdir}"/${pkgbase}-${pkgver}-stable-export-templates/bin/godot.x11.opt.64 "${pkgdir}"/usr/share/godot/templates/
  install -Dm644 "${srcdir}"/${pkgbase}-${pkgver}-stable-export-templates/bin/godot.x11.opt.debug.64 "${pkgdir}"/usr/share/godot/templates/
}

package_godot-headless-server() {
  provides=("${pkgbase}-headless-server")
  conflicts=("${pkgbase}-headless-server")
  depends=("${pkgbase}")
  license=('MIT')

  cd "${srcdir}"
  mkdir -p "${pkgdir}"/usr/share/godot/headless-server/

  install -Dm755 "${srcdir}"/${pkgbase}-${pkgver}-stable-headless-server/bin/godot.x11.opt.64 "${pkgdir}"/usr/share/godot/headless-server/godotd
  install -Dm755 "${srcdir}"/${pkgbase}-${pkgver}-stable-headless-server/bin/godot.x11.opt.debug.64 "${pkgdir}"/usr/share/godot/headless-server/godotd_debug
}
